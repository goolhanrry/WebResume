<p align="center">
  <img with="100" height="100" src="https://github.com/goolhanrry/WebResume/blob/master/assets/icon.png" alt="icon">
</p>

<h1 align="center">An online resume management system</h1>

<p align="center">
  <a href="LICENSE"><img src="https://img.shields.io/badge/license-MIT-blue.svg" alt="License"></a>
  <br><br>
  <img width="800" src="https://github.com/goolhanrry/WebResume/blob/master/assets/WebResume_Screenshot1.png" alt="screenshot1">
  <br><br>
  <img width="800" src="https://github.com/goolhanrry/WebResume/blob/master/assets/WebResume_Screenshot2.png" alt="screenshot2">
</p>
